<?php declare(strict_types=1);

namespace JMSE\ProductChildren\Controller;

use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EqualsFilter;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class ProductChildrenController extends StorefrontController
{
    public EntityRepository $productRepository;

    public function __construct(EntityRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/store-api/product/{id}/children", name="store-api.product.children", methods={"GET"})
     */
    public function children(string $id)
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('parentId', $id));
        $context = Context::createDefaultContext();

        return $this->json($this->productRepository->search($criteria, $context));
    }
}